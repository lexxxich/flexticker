package flexticker

import (
	"context"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestFlex(t *testing.T) {

	t.Log("Тикер должен выполниться 8 раз в пределях интервала 100 мс с частотой 10 мс на тик. ((100мс-10мс/тик-1мс на работу программы)/10)")
	ticker := FlexTicker{
		Increment:  0,
		Coeff:      1.0,
		Bound:      -1,
		TicksLimit: -1,
	}

	var calls int = 0
	workerFn := func() bool {
		calls++
		return false
	}

	wg := sync.WaitGroup{}
	wg.Add(1)

	baseCtx, cancel := context.WithCancel(context.Background())

	go func() {
		defer wg.Done()
		ticker.Start(baseCtx, time.Millisecond*10, workerFn)
	}()

	time.Sleep(100 * time.Millisecond)

	cancel()

	wg.Wait()

	require.InDelta(t, 8, calls, 1)

	t.Log("Перезапуск тикера должен работать")

	baseCtx, cancel = context.WithCancel(context.Background())

	wg.Add(1)
	calls = 0
	go func() {
		defer wg.Done()
		ticker.Start(baseCtx, time.Millisecond*10, workerFn)
	}()

	time.Sleep(100 * time.Millisecond)

	cancel()

	wg.Wait()

	require.InDelta(t, 8, calls, 1)

}

func TestFlex2(t *testing.T) {
	t.Log("Ограничение на максимальное количество тиков должно работать")

	ticker := FlexTicker{
		Increment:          0,
		Coeff:              1.0,
		Bound:              -1,
		TicksLimit:         7,
		StopOnBoundReached: false,
	}

	var calls int = 0
	workerFn := func() bool {
		calls++
		return false
	}

	ticker.Start(context.TODO(), time.Millisecond*10, workerFn)

	require.Equal(t, int(ticker.TicksLimit), calls)
}

func TestFlex3(t *testing.T) {
	t.Log("Фиксированное увеличение интервала тиков должно работать")

	ticker := FlexTicker{
		Increment:          50 * time.Millisecond,
		Coeff:              1.0,
		Bound:              -1,
		TicksLimit:         5,
		StopOnBoundReached: false,
	}

	var calls int = 0
	start := time.Now()
	workerFn := func() bool {
		calls++
		return false
	}

	ticker.Start(context.TODO(), time.Millisecond*50, workerFn)

	end := time.Now()

	delta := end.Sub(start)

	require.Equal(t, 5, calls)
	require.InDelta(t, (10+20+30+40+50)*5, delta/time.Millisecond, 20, "Допустимое отклонение времени выполнения 20мс не соответствует ожидаемому")
}

func TestFlex4(t *testing.T) {
	t.Log("Увеличение интервала тиков на указанный коэффициент должно работать")

	ticker := FlexTicker{
		Increment:          0,
		Coeff:              1.2,
		Bound:              -1,
		TicksLimit:         5,
		StopOnBoundReached: false,
	}

	var calls int = 0
	start := time.Now()
	workerFn := func() bool {
		calls++
		return false
	}

	ticker.Start(context.TODO(), time.Millisecond*50, workerFn)

	end := time.Now()

	delta := end.Sub(start)

	require.Equal(t, int(ticker.TicksLimit), calls)
	require.InDelta(t, 50+50*1.2+50*1.2*1.2+50*1.2*1.2*1.2+50*1.2*1.2*1.2*1.2, delta/time.Millisecond, 20)
}

func TestFlex5(t *testing.T) {
	t.Log("Ограничение максимального интервала тикера должно работать")

	ticker := FlexTicker{
		Increment:          50 * time.Millisecond,
		Coeff:              1.0,
		Bound:              175 * time.Millisecond,
		TicksLimit:         5,
		StopOnBoundReached: false,
	}

	var calls int = 0
	start := time.Now()
	workerFn := func() bool {
		calls++
		return false
	}

	ticker.Start(context.TODO(), time.Millisecond*50, workerFn)

	end := time.Now()

	delta := end.Sub(start)

	require.Equal(t, int(ticker.TicksLimit), calls)
	require.InDelta(t, 50+(50+50)+(50+50+50)+175+175, delta/time.Millisecond, 20)
}

func TestFlex6(t *testing.T) {
	t.Log("Остановка тикера при достижении максимального интервала срабатывания должна работать")

	ticker := FlexTicker{
		Increment:          50 * time.Millisecond,
		Coeff:              1.0,
		Bound:              175 * time.Millisecond,
		TicksLimit:         5,
		StopOnBoundReached: true,
	}

	var calls int = 0
	start := time.Now()
	workerFn := func() bool {
		calls++
		return false
	}

	ticker.Start(context.TODO(), time.Millisecond*50, workerFn)

	end := time.Now()

	delta := end.Sub(start)

	require.Equal(t, int(ticker.TicksLimit)-1, calls)
	require.InDelta(t, 50+(50+50)+(50+50+50)+175, delta/time.Millisecond, 20)
}

func TestFlex7(t *testing.T) {
	t.Log("Если воркер выполняется дольше чем длительность тика, то работа следующего воркера отменяется")

	ticker := FlexTicker{
		Increment:          0,
		Coeff:              1.0,
		Bound:              -1,
		TicksLimit:         5,
		StopOnBoundReached: false,
	}

	var calls int = 0
	workerFn := func() bool {
		calls++
		if calls > 0 {
			time.Sleep((100 + 10) * time.Millisecond)
		}
		return false
	}

	ticker.Start(context.TODO(), time.Millisecond*50, workerFn)

	require.Equal(t, int(ticker.TicksLimit)-3, calls)
}

func TestFlex8(t *testing.T) {
	t.Log("Остановка тикера по инициативе воркера должна работать")

	ticker := FlexTicker{
		Increment:          0,
		Coeff:              1.0,
		Bound:              -1,
		TicksLimit:         -1,
		StopOnBoundReached: false,
	}

	var calls int = 0
	workerFn := func() bool {
		calls++
		return calls > 2
	}

	ticker.Start(context.TODO(), time.Millisecond*50, workerFn)

	require.Equal(t, 3, calls)
}

func TestFlex9(t *testing.T) {
	t.Log("Валидация входных параметров при старте тикера должна работать")

	ticker := FlexTicker{
		Increment:          0,
		Coeff:              1.0,
		Bound:              -1,
		TicksLimit:         -1,
		StopOnBoundReached: false,
	}

	var calls int = 0
	workerFn := func() bool {
		calls++
		return calls > 2
	}

	ticker.Increment = -1

	err := ticker.Start(context.TODO(), time.Millisecond*50, workerFn)
	require.Error(t, err)

	ticker.Increment = 0
	ticker.Coeff = 0

	err = ticker.Start(context.TODO(), time.Millisecond*50, workerFn)
	require.Error(t, err)

	ticker.Coeff = -1
	ticker.Bound = 0
	err = ticker.Start(context.TODO(), time.Millisecond*50, workerFn)
	require.Error(t, err)
}

func TestFlex10(t *testing.T) {
	t.Log("Остановка тикера по инициативе внешней стороны должна работать")

	ticker := FlexTicker{
		Increment:          0,
		Coeff:              1.0,
		Bound:              -1,
		TicksLimit:         -1,
		StopOnBoundReached: false,
	}

	var calls int = 0
	workerFn := func() bool {
		calls++
		return false
	}

	go func() {
		time.Sleep(200 * time.Millisecond)
		ticker.Stop()
	}()

	ticker.Start(context.TODO(), time.Millisecond*50, workerFn)

	require.Equal(t, 3, calls)
}
