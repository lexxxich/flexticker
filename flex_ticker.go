package flexticker

import (
	"context"
	"errors"
	"sync"
	"time"
)

// Создает новый экземпляр тикера с настройками по-умолчанию (для тикера с постоянным интервалом тиков)
func New() *FlexTicker {
	return &FlexTicker{
		Increment:          0,
		Coeff:              1.0,
		Bound:              -1,
		TicksLimit:         -1,
		StopOnBoundReached: false,
	}
}

// Тикер. Можно создавать экземпляры этой структры, но внимательно обращать внимание на настройки
type FlexTicker struct {
	timer         *time.Timer
	contextCancel context.CancelFunc
	started       bool

	workerProcessing bool
	workerChannel    chan bool

	currentDuration time.Duration
	// totalTicks      int32

	Increment  time.Duration // Постоянная величина, на которую возрастает интервал тикера по умолчанию 0
	Coeff      float64       // Коэффициент, на который возрастает интервал тикера. Коэффициент должен быть положительным числом
	Bound      time.Duration // Максимальный интервал, которого может достичь тикер.
	TicksLimit int32         // Максимальное количество тиков, которое должен проработать тикер

	// Завершать ли работу тикера по достижении макисмального интервала тиков.
	// Функция-вокер при этом будет вызвана один раз и тикер завершен
	StopOnBoundReached bool
}

// Первый вызов воркера будет выполнен ПОСЛЕ указанного интервала времени
//
// Применение коэффициентов изменения интервала времени проходит так: сначала применяется инкремент, а на
// получившееся значение применяется множитель
//
// Если требуется остановка тикера по достижении определенного максимального интервала тиков,
// то важным моментом является то, что в этом случае воркер будет выполнен в последниф раз и тикер остановится.
//
// Воркеры вызываются в параллельном потоке, чтобы не оказывть влияние на время очередного тика.
// Если в момент работы медленного воркера произойдет очередной тик, то вызов нового воркера будет проигнорирован.
func (t *FlexTicker) Start(parentCtx context.Context, interval time.Duration, worker func() (shouldStop bool)) error {

	if t.started {
		return errors.New("тикер уже запущен. Повторный запуск невозможен")
	}

	// Базовая валидация
	if t.Coeff <= 0 {
		return errors.New("коэффициент увеличения интервала тикера должен быть положительным")
	}

	if t.Bound < -1 || t.Bound == 0 {
		return errors.New("максимальный интервал тикера должен быть положительным")
	}

	if t.TicksLimit < -1 || t.TicksLimit == 0 {
		return errors.New("ограничение по количеству тиков должно быть положительным числом")
	}

	if t.Increment < 0 {
		return errors.New("прирост значения тикера должен быть неотрицательным")
	}

	wg := sync.WaitGroup{}

	if t.workerChannel == nil {
		t.workerChannel = make(chan bool)
	}

	t.started = true

	ctx, cancel := context.WithCancel(parentCtx)
	t.contextCancel = cancel

	var totalTicks int32 = 0
	t.currentDuration = interval

	t.timer = time.NewTimer(interval)

	defer func() {
		t.started = false
		t.contextCancel()

		if t.workerChannel != nil {
			close(t.workerChannel)
			t.workerChannel = nil

		}

		wg.Wait()

	}()

	// Ожидаем тика
	for {
		select {
		case <-t.timer.C:

			// Вызов воркера в фоновом процессе
			if !t.workerProcessing {
				wg.Add(1)
				t.workerProcessing = true
				go t.doWorker(worker, &wg)
			}

			// Вызов таймера
			// if worker() {
			// 	// Если воркер вернул true - завершаем работу тикера
			// 	return nil
			// }

			// Если необходимо остановиться по достижении максимального интервала - делаем это
			if t.Bound != -1 && t.currentDuration >= t.Bound && t.StopOnBoundReached {
				return nil
			}
			// После вызова таймера увеличиваем интервалы если не достигнута верхняя граница интервала
			t.currentDuration += t.Increment
			t.currentDuration = time.Duration(float64(t.currentDuration) * t.Coeff)
			if (t.Bound != -1) && (t.currentDuration >= t.Bound) {
				t.currentDuration = t.Bound

			}

			// Проверяем необходимость работы тикера по достижению лимита тиков
			if (t.TicksLimit != -1) && (totalTicks+1 >= t.TicksLimit) {
				// Если нужно уведомить клиента о завершении тикера по причине
				return nil
			} else {
				totalTicks++
			}
			// Запускаем новый тик таймера
			t.timer.Reset(t.currentDuration)

		case <-ctx.Done():
			// По завершении родительского контекста завершаем работу тикера
			t.timer.Stop()
			return nil

		case <-t.workerChannel:
			// При необходимости завершения работы тикера по инициативе воркера - выходим
			return nil
		}
	}
}

// Останавливает тикер.
//
// Если в момент остановки тикера выполнялся воркер, то реально тикер остановится только по завершении
// работы воркера
func (t *FlexTicker) Stop() {
	if t.started {
		t.contextCancel()
	}
}

func (t *FlexTicker) doWorker(worker func() bool, wg *sync.WaitGroup) {

	rv := worker()

	wg.Done()
	if rv {
		t.workerChannel <- true
	}

	t.workerProcessing = false

}

// Возвращает факт того, запущен ли в данный момент тикер (true) или нет (false)
func (t *FlexTicker) Started() bool {
	return t.started
}