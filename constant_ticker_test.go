package flexticker

import (
	"context"
	"fmt"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestTicker(t *testing.T) {
	var err error
	t.Log("Тикер должен вызвать функцию-воркер 1 раз и затем завершиться по инициативе воркера")

	calls := 0

	worker := func() bool {
		calls++
		return calls >= 5
	}

	ticker := ConstantTicker{}

	ticker.Start(context.TODO(), time.Millisecond*10, worker)

	require.Equal(t, calls, 5)

	t.Log("Последовательный запуск двух тикеров не должен вызывать ошибку")

	calls = 0
	err = ticker.Start(context.TODO(), time.Millisecond*10, worker)
	require.NoError(t, err)
	err = ticker.Start(context.TODO(), time.Millisecond*10, worker)
	require.NoError(t, err)
	require.Equal(t, 6, calls)

	t.Log("Завершение контекста завершает тикер")

	calls = 0
	infinite := func() bool {
		calls++
		return false
	}

	ctx, cancel := context.WithCancel(context.TODO())
	// ticker.Context = ctx

	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		time.Sleep(time.Millisecond * 100)
		cancel()
	}()

	ticker.Start(ctx, time.Millisecond*10, infinite)

	wg.Wait()

	require.Equal(t, 10, calls, "Ожидалось 10 вызовов (100 мс/10 мс на каждый вызов метода infinte)")
	require.False(t, ticker.started)

	t.Log("остановка тикера вручную через метод Stop останавливает тикер")

	calls = 0
	// ticker.Context = context.TODO()
	wg.Add(1)
	go func() {
		defer wg.Done()
		err = ticker.Start(context.TODO(), time.Millisecond*10, infinite)
	}()

	time.Sleep(time.Millisecond * 65)
	ticker.Stop()

	wg.Wait()
	require.NoError(t, err)

	// time.Sleep(time.Millisecond * 100)

	require.Equal(t, false, ticker.started)
	require.Equal(t, 6, calls)

	t.Log("Последовательный вызов Stop ничего не делает ")

	ticker.Stop()
	ticker.Stop()

	t.Log("Запуск тикера без контекста спокойно выполняется")
	// ticker.Context = nil

	// calls = 0
	// err = ticker.Start(worker)

	// require.NoError(t, err)
	// require.Equal(t, 5, calls)
}

func TestTicker2(t *testing.T) {
	t.Log("Запуск ране запущенного тикера перезапускает его")

	calls := 0

	worker := func() bool {
		calls++
		return calls >= 5
	}

	ticker := ConstantTicker{}

	calls = 0

	wg := sync.WaitGroup{}

	wg.Add(1)

	var err error
	go func() {
		defer wg.Done()
		err = ticker.Start(context.TODO(), time.Millisecond*10, worker)
	}()
	ticker.Start(context.TODO(), time.Millisecond*10, worker)

	wg.Wait()

	require.Error(t, err, "Запуск второго тикера при запущенном первом вызывает ошибку")

	require.Equal(t, false, ticker.started)

	require.Equal(t, 5, calls, "Ожидалось 5 вызовов. Первый тикер не должен успеть вызвать метод и сразу отмениться")

}

func TestContext(t *testing.T) {
	t.Log("Остановка родительского контекста останавливает тикер")

	ticker1 := &ConstantTicker{}

	ticker2 := &ConstantTicker{}

	wg := sync.WaitGroup{}

	baseContext, cancel := context.WithCancel(context.Background())

	childContext, childCancel := context.WithCancel(baseContext)
	defer childCancel()

	infnite := func() bool {
		return false
	}

	wg.Add(2)
	var t1err, t2err error

	go func() {
		defer wg.Done()
		t1err = ticker1.Start(childContext, time.Millisecond*50, infnite)

		fmt.Println("ticker1 stopped")
	}()
	go func() {
		defer wg.Done()
		t2err = ticker2.Start(baseContext, time.Millisecond*50, infnite)
		fmt.Println("ticker2 stopped")
	}()

	time.Sleep(200 * time.Millisecond)
	require.True(t, ticker1.IsActive())
	require.True(t, ticker2.IsActive())
	cancel()
	wg.Wait()

	require.NoError(t, t1err)
	require.NoError(t, t2err)

	require.False(t, ticker1.IsActive())
	require.False(t, ticker2.IsActive())
}
