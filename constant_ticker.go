package flexticker

import (
	"context"
	"errors"
	"time"
)

type ConstantTicker struct {
	ticker        *time.Ticker
	contextCancel context.CancelFunc
	started       bool
}

// Запускает периодический вызов функции-воркера с интервалом,
// заданным параметром Interval экземпляра тикера.
//
// Остановка тикера выполняется при наступлении одного из следующих условий:
//
// - функция-воркер вернула true
//
// - Был вызван метод Stop(). Важно что его можно запустить только из фонового
// (параллельного процесса или горутины)
//
// - Контекст завершил свою работу
func (t *ConstantTicker) Start(parentCtx context.Context, interval time.Duration, worker func() bool) error {

	if t.started {
		// Если тикер ранее запущен - выходим с ошибкой
		return errors.New("тикер уже запущен. Повторный запуск невозможен")
	}

	ctx, cancel := context.WithCancel(parentCtx)
	t.contextCancel = cancel

	defer t.contextCancel()

	t.started = true
	if t.ticker == nil {
		t.ticker = time.NewTicker(interval)
	} else {
		t.ticker.Reset(interval)
	}

	for {
		select {
		case <-t.ticker.C:
			// При тике вызываем метод
			if worker() {
				// Если воркер вернул истину - завершаем процесс
				if t.started {
					t.ticker.Stop()
					t.contextCancel()
				}
				t.started = false
				return nil
			}
		case <-ctx.Done():
			// При завершении контекста завершаем тикер
			if t.started {
				t.ticker.Stop()
			}
			t.started = false
			return nil

		}

	}

}

// Останавливает тикер, если он ранее не был запущен
func (t *ConstantTicker) Stop() {
	if !t.started {
		return
	}

	t.contextCancel()
}

// Возвращает статус того что тикер в рабочем состоянии
func (t *ConstantTicker) IsActive() bool {
	return t.started
}
